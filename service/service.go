package service

import "gitlab.com/Neel.Wankhede/provider/models"

type Service interface {
	GetUser(id int) (user *models.User, err error)
}
