package service

import (
	"errors"

	"gitlab.com/Neel.Wankhede/provider/models"
)

type ServiceImpl struct {
	db DB
}

func New(db DB) (serviceImpl *ServiceImpl, err error) {
	if db == nil {
		err = errors.New("db is nil")
		return
	}
	serviceImpl = &ServiceImpl{
		db: db,
	}
	return
}

func (s *ServiceImpl) GetUser(id int) (user *models.User, err error) {
	user, err = s.db.GetUser(id)
	return
}
