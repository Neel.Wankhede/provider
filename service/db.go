package service

import "gitlab.com/Neel.Wankhede/provider/models"

type DB interface {
	GetUser(id int) (user *models.User, err error)
}
