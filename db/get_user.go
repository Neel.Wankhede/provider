package db

import (
	"errors"

	"gitlab.com/Neel.Wankhede/provider/models"
)

func (dbImpl DBImpl) GetUser(id int) (user *models.User, err error) {
	for _, u := range dbImpl.users {
		if u.ID == id {
			user = &u
			return
		}
	}
	err = errors.New("user not found")
	return
}
