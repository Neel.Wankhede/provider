package db

import "gitlab.com/Neel.Wankhede/provider/models"

type DBImpl struct {
	users []models.User
}

func New() (dbImpl *DBImpl, err error) {
	dbImpl = &DBImpl{
		[]models.User{
			{
				FirstName: "Itachi",
				LastName:  "Uchiha",
				ID:        1,
				Type:      "SharinganUser",
				Username:  "itachi",
			},
			{
				FirstName: "Naruto",
				LastName:  "Uzumaki",
				ID:        2,
				Type:      "TailBeastUser",
				Username:  "naruto",
			},
			{
				FirstName: "Minato",
				LastName:  "Namikaze",
				ID:        3,
				Type:      "FlyingRaiginUser",
				Username:  "fourthHokage",
			},
		},
	}
	return
}
