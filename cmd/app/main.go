package main

import (
	"log"
	"time"

	"gitlab.com/Neel.Wankhede/provider/db"
	"gitlab.com/Neel.Wankhede/provider/server"
	"gitlab.com/Neel.Wankhede/provider/service"
)

func main() {
	config := server.Config{
		RequestTimeout: 200 * time.Millisecond,
		Port:           8080,
	}

	db, err := db.New()
	if err != nil {
		log.Fatal(err)
	}

	service, err := service.New(db)
	if err != nil {
		log.Fatal(err)
	}

	server, err := server.New(&config, service)
	if err != nil {
		log.Fatal(err)
	}

	err = server.Start()
	if err != nil {
		log.Fatal("unxepected shutdown of app: ", err)
	}

}
