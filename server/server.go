package server

type Server interface {
	Start() (err error)
}
