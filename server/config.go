package server

import "time"

type Config struct {
	RequestTimeout time.Duration
	Port           uint
}
