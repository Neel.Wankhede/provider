package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/Neel.Wankhede/provider/service"
)

type ServerImpl struct {
	config  *Config
	server  *http.Server
	service service.Service
}

func New(config *Config, service service.Service) (serverImpl *ServerImpl, err error) {
	if config == nil {
		err = errors.New("server config is nil")
		return
	}
	if service == nil {
		err = errors.New("service is nil")
		return
	}

	r := mux.NewRouter()

	r.HandleFunc("/users/{id}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		idstr, ok := vars["id"]
		if !ok {
			errResp := ErrorResp{"id not found"}
			resp, _ := json.Marshal(errResp)
			w.Write(resp)
			w.WriteHeader(http.StatusNotFound)
			return
		}
		id, _ := strconv.Atoi(idstr)
		user, err := service.GetUser(id)
		if err != nil {
			errResp := ErrorResp{err.Error()}
			resp, _ := json.Marshal(errResp)
			w.Write(resp)
			w.WriteHeader(http.StatusNotFound)
			return
		}

		u, _ := json.Marshal(user)
		w.Write(u)
		w.WriteHeader(http.StatusOK)

	})

	serverImpl = &ServerImpl{
		config: config,
		server: &http.Server{
			Handler: r,
			Addr:    fmt.Sprintf(":%d", config.Port),
		},
		service: service,
	}

	return
}

func (s *ServerImpl) Start() (err error) {
	err = s.server.ListenAndServe()
	return
}
